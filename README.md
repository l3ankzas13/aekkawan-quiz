# Aekkawan JS Quiz

### Clone Project
```
git clone https://gitlab.com/l3ankzas13/aekkawan-quiz.git
cd aekkawan-quiz
```

### Project Structure
```
- Quiz01 source code is in folder /src/Quiz01
- Quiz02 source code is in folder /src/Quiz02
- Quiz03 source code is in folder /src/Quiz03
```

### Install dependencies with YARN
```
yarn install
```

### Run Application for See Quiz01 and Quiz02
```
yarn start
```

### Go to page
```
default start with url http://localhost:3000

- You can see quiz01 with url http://localhost:3000/quiz01
- You can see quiz03 with url http://localhost:3000/bookinngs/today?roomId=A101
```

### Test Quiz02 with Jest (02 - Venue Booking System (Javascript Test))
```
yarn test:quiz02
```


## Libraries used in project
```
- React (Create project with Create React App)
- React-router-dom (for use router)
- Jest (for test)
```

