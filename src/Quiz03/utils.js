export const monthConfig = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
export const dayConfig = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

export const convertTime = (time) => {
  const date = new Date(time)
  const hours = date.getHours().toString()
  const minutes = date.getMinutes().toString()
  const displayHours = `${hours.length === 2 ? hours : '0' + hours}`
  const displayMinutes = `${minutes.length === 2 ? minutes : 0 + minutes}`
  return `${displayHours}:${displayMinutes}`
}

export const getDateOnly = (date) => {
  const dateValue = new Date(date)
  const month = `${dateValue.getMonth() + 1}`
  const day = dateValue.getDate().toString()
  const fullMonth = month.length > 1 ? month : `0${month}`
  const fullDay = day.length > 1 ? day : `0${day}`
  return `${dateValue.getFullYear()}${fullMonth}${fullDay}`
}

export const transformDate = (data) => {
  const groupDate = data.reduce((acc, item) => {
    const date = getDateOnly(item.startTime)
    const hasDateInPrev = acc[date]
    if (hasDateInPrev) {
      return { ...acc, [date]: [...acc[date], item] }
    }
    return { ...acc, [date]: [item] }
  }, {})

  return Object.entries(groupDate).sort(([, a], [, b]) => b[0] - a[0])
}

export const isToday = (currentDate, someDate) => {
  const today = new Date(currentDate)
  return someDate.getDate() === today.getDate() &&
    someDate.getMonth() === today.getMonth() &&
    someDate.getFullYear() === today.getFullYear()
}

export const isNextDay = (currentDate, someDate) => {
  const today = new Date(currentDate)
  const dayRang = someDate.getDate() - today.getDate()
  return dayRang === 1 &&
    someDate.getMonth() === today.getMonth() &&
    someDate.getFullYear() === today.getFullYear()
}


export const createDateTitle = (currentDate, someDate) => {
  const year = someDate.substring(0, 4)
  const month = someDate.substring(4, 6)
  const day = someDate.substring(6, 8)
  const date = new Date(`${year}-${month}-${day}`)
  const dayName = dayConfig[date.getDay()].substring(0, 3)
  const monthName = monthConfig[date.getMonth()];
  const title = `${dayName}, ${date.getDate()} ${monthName}`
  if (isToday(currentDate, date)) return `Today (${title})`
  if (isNextDay(currentDate, date)) return `Tomorrow (${title})`
  return title
}

export const extractDate = (someDate) => {
  const date = new Date(someDate)
  return {
    date: date.getDate(),
    day: dayConfig[date.getDay()],
    month: monthConfig[date.getMonth()],
    year: date.getYear(),
  }
}