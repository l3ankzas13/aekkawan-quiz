import React from 'react';
import { convertTime } from '../utils'

const getTime = (value) => {
  if (value.includes('NaN')) return ''
  return value
}

export default function List({ data = [] }) {
  return (
    <>
      {data.map((item) => {
        const time = `${convertTime(item?.startTime)} - ${convertTime(item?.endTime)}`
        return (
          <div className="list" key={item?.id}>
            <span className="time">{`${getTime(time)}`}</span>
            <span className="title">{item?.title}</span>
          </div>
        )
      })}
    </>
  )
}