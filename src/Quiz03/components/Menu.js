import React from 'react';

export default function Menu({
  selected,
  list = [],
  onClick = () => { }
}) {
  return (
    <>
      {list.map(({ menu, id }) => (
        <div key={id} className="menu" onClick={() => onClick(id)}>
          <span className={selected === id ? 'active' : ''}>{menu}</span>
          <div className={selected === id ? 'active' : ''}></div>
        </div>
      ))}
    </>

  )
}