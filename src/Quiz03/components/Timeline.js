import React from 'react';
import {
  convertTime,
  transformDate,
  createDateTitle
} from '../utils'

const sortByStartTime = (a, b) => Date.parse(a.startTime) - Date.parse(b.startTime)

export default function TimeLine({ currentDate = '2019-09-28', data = [] }) {
  const timelineList = transformDate(data)
  return (
    <div className="timeline-body">
      {timelineList.map(([date, subDate]) => (
        <>
          <div className="timeline-section-header">{createDateTitle(currentDate, date)}</div>
          <div className="timeline">
            <div>
              {subDate.sort(sortByStartTime).map((value) => (
                <section>
                  <ul>
                    <li>
                      <span className="time black">{`${convertTime(value.startTime)} - ${convertTime(value.endTime)}`}</span>
                      <span className="title black">{value.title}</span>
                    </li>
                  </ul>
                </section>
              ))}
            </div>
          </div>
        </>
      ))}
    </div>
  )
}