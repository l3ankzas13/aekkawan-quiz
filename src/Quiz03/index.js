import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";
import { List, Timeline, Menu } from './components'
import { extractDate } from './utils'
import {
  getBookingForMonth,
  getBookingForToday,
  getBookingsForWeek
} from '../Quiz02';

import './quiz03.css';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export default function Quiz03() {
  let query = useQuery();
  const roomId = query.get("roomId")
  const currentDate = '2019-09-28'
  const [selectedMenu, setSelectMenu] = useState(1)
  const [todayBooking, setTodayBooking] = useState([])
  const [booking, setBooking] = useState([])

  useEffect(() => {
    let data = []
    if (selectedMenu === 1) {
      data = getBookingsForWeek({ roomId })
    }
    if (selectedMenu === 2) {
      data = getBookingsForWeek({ roomId, nextWeek: true })
    }
    if (selectedMenu === 3) {
      data = getBookingForMonth({ roomId })
    }
    setBooking(data)
  }, [selectedMenu, roomId])


  useEffect(() => {
    const data = getBookingForToday({ roomId })
    if (data && data.length > 0) {
      const sortable = data.sort((a, b) => Date.parse(a.startTime) - Date.parse(b.startTime))
      setTodayBooking(sortable)
    }
  }, [roomId])

  const { date, day, month } = extractDate(currentDate)

  return (
    <div className="quiz03-container">
      <div className="card">
        <div className="box-1">
          <div className="section-1">
            <div className="room-wrapper">
              <div className="room">
                <span>{roomId}</span>
              </div>
            </div>
            <div className="content">
              <div className="current">
                <span className="mb-20">Upcoming</span>
                <h1 className="light-weight opacity-0-5">{day}</h1>
                <h1 className="light-weight mb-60">{`${date} ${month}`}</h1>
                <List data={[todayBooking[0]]}
                />
              </div>
            </div>
          </div>
          <div className="section-2">
            <List data={todayBooking.slice(1)}
            />
          </div>
        </div>
        <div className="box-2">
          <div className="timeline-header">
            <div className="header">
              <Menu list={[
                { id: 1, menu: 'THIS WEEK' },
                { id: 2, menu: 'NEXT WEEK' },
                { id: 3, menu: 'WHOLE MONTH' },
              ]}
                selected={selectedMenu}
                onClick={setSelectMenu}
              />
            </div>
          </div>
          <Timeline
            currentDate={currentDate}
            data={booking}
          />
        </div>
      </div>
    </div>
  )
}