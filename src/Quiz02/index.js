import bookingData from './booking.json'

//utils function
const isValidBetweenDate = (form, to, search) => {
  const formTime = Date.parse(form);
  const toTime = Date.parse(to);
  const searchTime = Date.parse(search);
  return searchTime >= formTime && searchTime <= toTime
}

const getNextWeek = (value) => {
  const date = new Date(value);
  const today = date.getDate();
  const dayOfTheWeek = date.getDay();
  const startDate = new Date(date.setDate(today - dayOfTheWeek + 7));
  const endDate = new Date(date.setDate(today - dayOfTheWeek + 13));
  return {
    startDate,
    endDate
  }
}

const getCurrentMonth = (value) => {
  const date = new Date(value);
  const startDate = new Date(date.getFullYear(), date.getMonth(), 1);
  const endDate = new Date(date.getFullYear(), date.getMonth() + 1, 1);
  return {
    startDate,
    endDate
  }
}

// main function
const checkAvailability = (roomId, startTime, endTime) => {
  if (Date.parse(endTime) < Date.parse(startTime)) return false;
  const isAvailable = bookingData.some((booking) => {
    const startDateCheck = isValidBetweenDate(booking.startTime, booking.endTime, startTime)
    const endDateCheck = isValidBetweenDate(booking.startTime, booking.endTime, endTime)
    return roomId === booking.roomId && (startDateCheck || endDateCheck)
  })
  return !isAvailable
}

const getBookingForMonth = ({ roomId }) => {
  const { startDate, endDate } = getCurrentMonth('2019-09-28')
  return bookingData.filter((booking) => {
    const validStartTime = isValidBetweenDate(startDate, endDate, booking.startTime)
    return booking.roomId === roomId && validStartTime
  })
}

const getBookingForToday = ({ roomId }) => {
  const startOfDay = new Date('2019-09-28')
  const endOfDay = new Date('2019-09-28')
  endOfDay.setHours(23, 59, 59, 999);
  return bookingData.filter((booking) => {
    const validStartTime = isValidBetweenDate(startOfDay, endOfDay, booking.startTime)
    return booking.roomId === roomId && validStartTime
  })
}

const getBookingsForWeek = ({ roomId, nextWeek = false }) => {
  const currentDate = new Date("2019-09-28");
  let firstDateOfWeek
  let lastDateOfWeek
  if (nextWeek) {
    const { startDate, endDate } = getNextWeek(currentDate)
    firstDateOfWeek = startDate
    lastDateOfWeek = endDate
  } else {
    const firstDayOfWeek = currentDate.getDate() - currentDate.getDay()
    const lastDayOfWeek = firstDayOfWeek + 7
    firstDateOfWeek = new Date(currentDate.setDate(firstDayOfWeek))
    lastDateOfWeek = new Date(currentDate.setDate(lastDayOfWeek))
  }

  return bookingData.filter((booking) => {
    const validStartTime = isValidBetweenDate(firstDateOfWeek, lastDateOfWeek, booking.startTime)
    return booking.roomId === roomId && validStartTime
  })
}

/* testing
  checkAvailability('A101', '2019-09-28 13:00:00', '2019-09-28 14:00:00') //false
  checkAvailability('A102', '2019-09-28 13:00:00', '2019-09-28 14:00:00') //false

  console.log(checkAvailability('A101', '2019-10-29 15:00:00', '2019-10-30 14:00:00'))
  console.log(getBookingsForWeek({ roomId: 'A101', nextWeek: true }))
  console.log(getBookingForMonth({ roomId: 'A101' }))
testing */

export {
  checkAvailability,
  getBookingsForWeek,
  getBookingForToday,
  getBookingForMonth
}
