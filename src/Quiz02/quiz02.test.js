import {
  checkAvailability,
  getBookingsForWeek,
  getBookingForToday,
  getBookingForMonth
} from './index'

describe('Check Room Availability', () => {

  it('A101 not available when startTime is `2019-09-28 13:00:00` and endTime is 2019-09-28 14:00:00', () => {
    const available = checkAvailability('A101', '2019-09-28 13:00:00', '2019-09-28 14:00:00');
    expect(available).toBe(false);
  })

  it('A101 not available when startTime is `2019-09-28 13:50:00` and endTime is 2019-09-28 18:00:00', () => {
    const available = checkAvailability('A101', '2019-09-28 14:10', '2019-09-28 18:00');
    expect(available).toBe(false);
  })

  it('A101 available when startTime is `2019-09-28 19:00:00` and endTime is 2019-09-30 18:00:00', () => {
    const available = checkAvailability('A101', '2020-09-28 19:00:00', '2020-09-30 18:00:00');
    expect(available).toBe(true);
  })

  it('Auditorium not available when startTime is `2019-09-21 09:00:00` and endTime is 2019-09-22 09:00:00', () => {
    const available = checkAvailability('Auditorium', '2019-09-21 09:00:00', '2019-09-22 09:00:00');
    expect(available).toBe(false);
  })

  it('Auditorium not available when startTime is `2019-09-23 20:00:00` and endTime is 2019-09-24 09:00:00', () => {
    const available = checkAvailability('Auditorium', '2019-09-23 20:00:00', '2019-09-24 09:00:00');
    expect(available).toBe(true);
  })
})

describe('Get Room List', () => {
  it('A101 booking in current week is 4 booking', () => {
    const booking = getBookingsForWeek({ roomId: 'A101' });
    expect(booking).toHaveLength(4);
  })

  it('A101 booking in next week is 3 booking', () => {
    const booking = getBookingsForWeek({ roomId: 'A101', nextWeek: true });
    expect(booking).toHaveLength(3);
  })

  it('A102 booking in current week is empty', () => {
    const booking = getBookingsForWeek({ roomId: 'A102' });
    expect(booking).toHaveLength(0);
  })

  it('A101 booking in current month is 6 booking', () => {
    const booking = getBookingForMonth({ roomId: 'A101' });
    expect(booking).toHaveLength(6);
  })

  it('A102 booking in current month is 2 booking', () => {
    const booking = getBookingForMonth({ roomId: 'A102' });
    expect(booking).toHaveLength(2);
  })

  it('A101 booking in today is 4 booking', () => {
    const booking = getBookingForToday({ roomId: 'A101' });
    expect(booking).toHaveLength(4);
  })

})