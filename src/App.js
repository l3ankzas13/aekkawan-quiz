import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './App.css';

import Quiz01 from './Quiz01/index';
import Quiz03 from './Quiz03/index';

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/quiz01">Quiz01 - (01 - CSS Test)</Link>
            </li>
            <li>
              <Link to="/bookinngs/today?roomId=A101">Quiz03 (A101 today with current date is 2019/09/28)</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/quiz01">
            <Quiz01 />
          </Route>
          <Route path="/bookinngs/today">
            <Quiz03 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
