import React, { useEffect, useState } from 'react';

export default function Quiz01() {
  const [pictureList, setPictureList] = useState([])
  useEffect(() => {
    const fetchPicture = async () => {
      const pictureList = await (await fetch('https://picsum.photos/v2/list')).json()
      setPictureList(pictureList)
    }
    fetchPicture()
  }, [])


  return (
    <div className="container">
      <div className="gallery">
        {pictureList.map(({ id, author, download_url }) => (
          <img className="picture" key={id} src={download_url} alt={author} />
        ))}
      </div>
    </div>
  )
}